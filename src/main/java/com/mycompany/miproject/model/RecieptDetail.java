/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.miproject.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author pattarapon
 */
public class RecieptDetail {
    private int id;
    private int productId;
    private String productName;
    private float productPrice;
    private int qty;
    private float totalPrice;
    private int recieptId;
    private String productType;
    private String productSweet;

    public RecieptDetail(int id, int productId, String productName, float productPrice, int qty, float totalPrice, int recieptId, String type, String sweet) {
        this.id = id;
        this.productId = productId;
        this.productName = productName;
        this.productPrice = productPrice;
        this.qty = qty;
        this.totalPrice = totalPrice;
        this.recieptId = recieptId;
        this.productType = type;
        this.productSweet = sweet;
    }
    public RecieptDetail(int productId, String productName, float productPrice, int qty, float totalPrice, int recieptId, String type, String sweet) {
        this.id = -1;
        this.productId = productId;
        this.productName = productName;
        this.productPrice = productPrice;
        this.qty = qty;
        this.totalPrice = totalPrice;
        this.recieptId = recieptId;
        this.productType = type;
        this.productSweet = sweet;
    }
    public RecieptDetail(int productId, String productName, float productPrice, int qty, float totalPrice, int recieptId) {
        this.id = -1;
        this.productId = productId;
        this.productName = productName;
        this.productPrice = productPrice;
        this.qty = qty;
        this.totalPrice = totalPrice;
        this.recieptId = recieptId;
        this.productType = "-";
        this.productSweet = "-";
    }
    
    public RecieptDetail() {
        this.id = -1;
        this.productId = 0;
        this.productName = "";
        this.productPrice = 0;
        this.qty = 0;
        this.totalPrice = 0;
        this.recieptId = 0;
        this.productType = "";
        this.productSweet = "";
    }

    

    public int getId() {
        return id;
    }

    public void setId(int Id) {
        this.id = Id;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public float getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(float productPrice) {
        this.productPrice = productPrice;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
        totalPrice = qty*productPrice;
    }

    public float getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(float totalPrice) {
        this.totalPrice = totalPrice;
    }

    public int getRecieptId() {
        return recieptId;
    }

    public void setRecieptId(int recieptId) {
        this.recieptId = recieptId;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String type) {
        this.productType = type;
    }

    public String getProductSweet() {
        return productSweet;
    }

    public void setProductSweet(String sweet) {
        this.productSweet = sweet;
    }

    @Override
    public String toString() {
        return "RecieptDetail{" + "id=" + id + ", productId=" + productId + ", productName=" + productName + ", productPrice=" + productPrice + ", qty=" + qty + ", totalPrice=" + totalPrice + ", recieptId=" + recieptId + ", productType=" + productType + ", productSweet=" + productSweet + '}';
    }


     public static RecieptDetail fromRS(ResultSet rs) {
        RecieptDetail recieptDetail = new RecieptDetail();
        try {
            recieptDetail.setId(rs.getInt("reciept_detail_id"));
            recieptDetail.setProductId(rs.getInt("product_id"));
            recieptDetail.setProductName(rs.getString("product_name"));
            recieptDetail.setProductPrice(rs.getFloat("product_price"));
            recieptDetail.setQty(rs.getInt("qty"));
            recieptDetail.setTotalPrice(rs.getFloat("total_price"));
            recieptDetail.setRecieptId(rs.getInt("reciept_id"));
            recieptDetail.setProductType(rs.getString("product_type"));
            recieptDetail.setProductSweet(rs.getString("product_sweet_level"));
        } catch (SQLException ex) {
            Logger.getLogger(RecieptDetail.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return recieptDetail;
    }
}
