/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.mycompany.miproject.compronent;

import com.mycompany.miproject.model.Product;

/**
 *
 * @author pattarapon
 */
public interface BuyProductable {
    public void buy(Product product,int qty,String type,String sweet);
    public void buy(Product product,int qty);
}
