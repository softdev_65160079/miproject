/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.miproject.model;

import com.mycompany.miproject.Dao.CustomerDao;
import com.mycompany.miproject.Dao.UserDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author pattarapon
 */
public class PromotionDetail {
    private int id;
    private int promotionId;
    private int productId;;
    private float discount;

    public PromotionDetail(int id, int promotionId, int productId, float discount) {
        this.id = id;
        this.promotionId = promotionId;
        this.productId = productId;
        this.discount = discount;
    }
    
    public PromotionDetail(int promotionId, int productId, float discount) {
        this.id = -1;
        this.promotionId = promotionId;
        this.productId = productId;
        this.discount = discount;
    }
        
    public PromotionDetail() {
        this.id = -1;
        this.promotionId = 0;
        this.productId = 0;
        this.discount = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPromotionId() {
        return promotionId;
    }

    public void setPromotionId(int promotionId) {
        this.promotionId = promotionId;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public float getDiscount() {
        return discount;
    }

    public void setDiscount(float discount) {
        this.discount = discount;
    }

    @Override
    public String toString() {
        return "PromotionDetail{" + "id=" + id + ", promotionId=" + promotionId + ", productId=" + productId + ", discount=" + discount + '}';
    }
    
    
    public static PromotionDetail fromRS(ResultSet rs) {
        PromotionDetail promotionDetail = new PromotionDetail();
        try {
            promotionDetail.setId(rs.getInt("promotionDetail_id"));
            promotionDetail.setPromotionId(rs.getInt("promotion_id"));
            promotionDetail.setProductId(rs.getInt("product_id"));
            promotionDetail.setDiscount(rs.getFloat("promotion_discount"));
        } catch (SQLException ex) {
            Logger.getLogger(PromotionDetail.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return promotionDetail;
    }
        
}
