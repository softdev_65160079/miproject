/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.miproject.model;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author pattarapon
 */
public class Promotion {
    private int id;
    private String name;
    private String start;
    private String con;
    private String end;
    private int status;
    private double discount;
    private int unit;
    private String product;
    private ArrayList<PromotionDetail> promotionDetails = new ArrayList<PromotionDetail>();
    public Promotion(int id, String name, String start, String con, String end, int status, double discount, int unit,String product) {
        this.id = id;
        this.name = name;
        this.start = start;
        this.con = con;
        this.end = end;
        this.status = status;
        this.discount = discount;
        this.unit = unit;
        this.product = product;
    }
    public Promotion(String name, String start, String con, String end, int status, double discount, int unit,String product) {
        this.id = -1;
        this.name = name;
        this.start = start;
        this.con = con;
        this.end = end;
        this.status = status;
        this.discount = discount;
        this.unit = unit;
        this.product = product;
    }
    public Promotion(String name, String start, String con, String end, int status, double discount, int unit) {
        this.id = -1;
        this.name = name;
        this.start = start;
        this.con = con;
        this.end = end;
        this.status = status;
        this.discount = discount;
        this.unit = unit;
        this.product = "";
    }
    public Promotion(String name, String start, String con, String end, int status, double discount,String product) {
        this.id = -1;
        this.name = name;
        this.start = start;
        this.con = con;
        this.end = end;
        this.status = status;
        this.discount = discount;
        this.unit = 0;
        this.product = product;
    }
    public Promotion(String name, String start, String con, String end, int status, double discount) {
        this.id = -1;
        this.name = name;
        this.start = start;
        this.con = con;
        this.end = end;
        this.status = status;
        this.discount = discount;
        this.unit = 0;
    }
    public Promotion() {
        this.id = -1;
        this.name = "";
        this.start = "";
        this.con = "";
        this.end = "";
        this.status = 0;
        this.discount = 0;
        this.unit = 0;
        this.product = null;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getCon() {
        return con;
    }

    public void setCon(String con) {
        this.con = con;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public int getUnit() {
        return unit;
    }

    public void setUnit(int unit) {
        this.unit = unit;
    }

    public ArrayList<PromotionDetail> getPromotionDetails() {
        return promotionDetails;
    }

    public void setPromotionDetails(ArrayList<PromotionDetail> promotionDetails) {
        this.promotionDetails = promotionDetails;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    @Override
    public String toString() {
        return "Promotion{" + "id=" + id + ", name=" + name + ", start=" + start + ", con=" + con + ", end=" + end + ", status=" + status + ", discount=" + discount + ", unit=" + unit + ", product=" + product + ", promotionDetails=" + promotionDetails + '}';
    }


    
    
    public static Promotion fromRS(ResultSet rs) {
        Promotion promotion = new Promotion();
        try {
            promotion.setId(rs.getInt("promotion_id"));
            promotion.setName(rs.getString("promotion_name"));
            promotion.setStart(rs.getString("promotion_start"));
            promotion.setCon(rs.getString("promotion_con"));
            promotion.setEnd(rs.getString("promotion_end"));
            promotion.setStatus(rs.getInt("promotion_status"));
            promotion.setDiscount(rs.getDouble("promotion_discount"));
            promotion.setUnit(rs.getInt("unit"));
            promotion.setProduct(rs.getString("product"));
        } catch (SQLException ex) {
            Logger.getLogger(Promotion.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return promotion;
    }

         
}
