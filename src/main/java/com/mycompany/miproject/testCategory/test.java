/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.miproject.testCategory;

import com.mycompany.miproject.Dao.PromotionDao;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;


/**
 *
 * @author pattarapon
 */
public class test {
    
    public static void main(String[] args) {
         LocalDate currentDate = LocalDate.now();
        
        // กำหนดรูปแบบของวันที่
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        
        // แปลง LocalDate เป็น String ในรูปแบบ "yyyy-MM-dd"
        String formattedDate = currentDate.format(formatter);
        
        // แสดงผลลัพธ์
        System.out.println("current date: " + formattedDate);
           
    }
   
        
    }
    
    

