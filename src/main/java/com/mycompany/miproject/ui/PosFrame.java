/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package com.mycompany.miproject.ui;

import com.mycompany.miproject.Dao.ProductDao;
import com.mycompany.miproject.Dao.PromotionDao;
import com.mycompany.miproject.Dao.UserDao;

import com.mycompany.miproject.model.Product;
import com.mycompany.miproject.model.Reciept;
import com.mycompany.miproject.model.RecieptDetail;

import com.mycompany.miproject.Service.RecieptService;
import com.mycompany.miproject.Service.UserService;
import static com.mycompany.miproject.Service.UserService.current;
import com.mycompany.miproject.compronent.BuyProductable;
import com.mycompany.miproject.compronent.ProductListPanel;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.Timer;
import javax.swing.table.AbstractTableModel;
import com.mycompany.miproject.model.Promotion;
import com.mycompany.miproject.model.User;
import java.util.Collections;
import java.util.Comparator;

/**
 *
 * @author User
 */
public class PosFrame extends javax.swing.JFrame implements BuyProductable {

    RecieptService recieptService = new RecieptService();
    Reciept reciept;
    private ProductDao productDao = new ProductDao();
    private ArrayList<Product> products = (ArrayList<Product>) productDao.getAll();
    private PromotionDao promotionDao = new PromotionDao();
    private ArrayList<Promotion> promotions = (ArrayList<Promotion>) promotionDao.getAll();
    private final ProductListPanel productListPanel;
    private Promotion promotion;
    private User currentUser;
    private boolean ready = false;
    private UserDao userDao = new UserDao();
    private float discount;
    private float dismax = 0;
    private float total;

    /**
     * Creates new form PosFrame
     */
    public PosFrame(User currentUser) {

        initComponents();
        currentUser = userDao.get(1);
        reciept = new Reciept();
        lblUserName.setText("User : " + currentUser.getName());
        lblStatus.setText("Status : " + currentUser.getRole(""));
        //reciept.setUser(UserService.getCurrent());
        Timer timer = new Timer(1000, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                timerActionPerformed(evt);
            }
        });
        // เริ่มต้น Timer
        timer.start();

        ImageIcon icon = new ImageIcon("./Logo.png");
        Image image = icon.getImage();
        Image newImage = image.getScaledInstance(107, 100, Image.SCALE_SMOOTH);
        icon.setImage(newImage);
        pCoffee.setIcon(icon);

        icon = new ImageIcon("./User" + currentUser.getId() + ".png");
        image = icon.getImage();
        int width = image.getWidth(null);
        int height = image.getHeight(null);
        newImage = image.getScaledInstance((int) (100 * ((float) width / height)), 100, Image.SCALE_SMOOTH);
        icon.setImage(newImage);
        pUser.setIcon(icon);

        tblReciepDetail.setModel(new AbstractTableModel() {
            String[] header = {"Name", "Price", "Qty", "Type", "Sweet", "TotalPrice"};

            @Override
            public String getColumnName(int column) {
                return header[column];
            }

            @Override
            public int getRowCount() {
                return reciept.getRecieptDetails().size();
            }

            @Override
            public int getColumnCount() {
                return 6;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                ArrayList<RecieptDetail> recieptDetails = reciept.getRecieptDetails();
                RecieptDetail recieptDetail = recieptDetails.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return recieptDetail.getProductName();
                    case 1:
                        return recieptDetail.getProductPrice();
                    case 2:
                        return recieptDetail.getQty();
                    case 3:
                        return recieptDetail.getProductType();
                    case 4:
                        return recieptDetail.getProductSweet();
                    case 5:
                        return recieptDetail.getTotalPrice();
                    default:
                        throw new AssertionError();
                }

            }

            @Override
            public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
                ArrayList<RecieptDetail> recieptDetails = reciept.getRecieptDetails();
                RecieptDetail recieptDetail = recieptDetails.get(rowIndex);
                if (columnIndex == 2) {
                    int qty = Integer.parseInt((String) aValue);
                    if (qty < 1) {
                        return;
                    }
                    recieptDetail.setQty(qty);
                    reciept.calculateTotal();
                    lblTotal.setText("Total: " + (reciept.getTotal() - discount));
                    refreshReciept();
                }

            }

            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                switch (columnIndex) {
                    case 2:
                        return true;

                    default:
                        return false;
                }
            }

        });
        productListPanel = new ProductListPanel();
        productListPanel.addOnBuyProduct(this);
        scrProductList.setViewportView(productListPanel);
    }

    private void timerActionPerformed(java.awt.event.ActionEvent evt) {

        LocalDateTime now = LocalDateTime.now();

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");

        lblTime.setText("" + now.format(formatter));
    }

    private void refreshReciept() {
        setDis();
        total = reciept.getTotal() - dismax;
        lblTotal.setText("Total: " + total);
        tblReciepDetail.revalidate();
        tblReciepDetail.repaint();

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        lblTime = new javax.swing.JLabel();
        lblUserName = new javax.swing.JLabel();
        lblStatus = new javax.swing.JLabel();
        pUser = new javax.swing.JLabel();
        pCoffee = new javax.swing.JLabel();
        lblHeader = new javax.swing.JLabel();
        scrProductList = new javax.swing.JScrollPane();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblReciepDetail = new javax.swing.JTable();
        lblTotal = new javax.swing.JLabel();
        lblDiscount = new javax.swing.JLabel();
        lblDiscountName = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        btnCalculate = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        btnTest = new javax.swing.JButton();
        btnMainManu = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel2.setBackground(new java.awt.Color(96, 76, 70));

        lblTime.setFont(new java.awt.Font("Segoe UI", 0, 24)); // NOI18N
        lblTime.setForeground(new java.awt.Color(255, 255, 255));
        lblTime.setText("Date");

        lblUserName.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lblUserName.setForeground(new java.awt.Color(255, 255, 255));
        lblUserName.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblUserName.setText("User : ภัทรพล สุดประเสริฐ");

        lblStatus.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lblStatus.setForeground(new java.awt.Color(255, 255, 255));
        lblStatus.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblStatus.setText("Status : Staff");

        pUser.setBackground(new java.awt.Color(96, 76, 70));
        pUser.setOpaque(true);

        pCoffee.setBackground(new java.awt.Color(96, 76, 70));
        pCoffee.setOpaque(true);

        lblHeader.setBackground(new java.awt.Color(255, 51, 51));
        lblHeader.setFont(new java.awt.Font("STLiti", 1, 40)); // NOI18N
        lblHeader.setForeground(new java.awt.Color(255, 255, 255));
        lblHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblHeader.setText("D-COFFEE");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pCoffee, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(52, 52, 52)
                .addComponent(lblHeader, javax.swing.GroupLayout.DEFAULT_SIZE, 291, Short.MAX_VALUE)
                .addGap(419, 419, 419)
                .addComponent(pUser, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(lblUserName, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 244, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblTime, javax.swing.GroupLayout.PREFERRED_SIZE, 244, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblStatus, javax.swing.GroupLayout.PREFERRED_SIZE, 244, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pCoffee, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(lblTime)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(lblUserName)
                                .addGap(3, 3, 3)
                                .addComponent(lblStatus))
                            .addComponent(pUser, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(32, 32, 32)
                .addComponent(lblHeader, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        tblReciepDetail.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        tblReciepDetail.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane2.setViewportView(tblReciepDetail);

        lblTotal.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lblTotal.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblTotal.setText("Total: 0");

        lblDiscount.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lblDiscount.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblDiscount.setText("Discount : 0.0");

        lblDiscountName.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lblDiscountName.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(scrProductList, javax.swing.GroupLayout.PREFERRED_SIZE, 501, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jScrollPane2))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(lblDiscountName, javax.swing.GroupLayout.PREFERRED_SIZE, 244, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lblTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 129, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(lblDiscount, javax.swing.GroupLayout.PREFERRED_SIZE, 176, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(125, 125, 125))))
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(scrProductList, javax.swing.GroupLayout.DEFAULT_SIZE, 629, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jScrollPane2)
                        .addGap(18, 18, 18)
                        .addComponent(lblTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblDiscount, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblDiscountName, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(39, 39, 39))))
        );

        jPanel5.setBackground(new java.awt.Color(153, 153, 153));

        btnCalculate.setText("Confirm");
        btnCalculate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCalculateActionPerformed(evt);
            }
        });

        jButton1.setText("Cancel");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        btnTest.setText("test");
        btnTest.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTestActionPerformed(evt);
            }
        });

        btnMainManu.setText("Main Manu");
        btnMainManu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMainManuActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnMainManu)
                .addGap(26, 26, 26)
                .addComponent(btnTest)
                .addGap(18, 18, 18)
                .addComponent(jButton1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnCalculate)
                .addContainerGap())
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnCalculate)
                    .addComponent(jButton1)
                    .addComponent(btnTest)
                    .addComponent(btnMainManu))
                .addGap(54, 54, 54))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnCalculateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCalculateActionPerformed
        System.out.println("" + reciept);
        recieptService.addNew(reciept);
        clearReciept();
//        RecieptDialog();
    }//GEN-LAST:event_btnCalculateActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        clearReciept();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void btnTestActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTestActionPerformed
//        RecieptDialog();
    }//GEN-LAST:event_btnTestActionPerformed

    private void btnMainManuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMainManuActionPerformed
        MainFrame mainFrame = new MainFrame(current);
        mainFrame.setVisible(true);  // เปิดหน้าต่าง MainFrame
        dispose();
    }//GEN-LAST:event_btnMainManuActionPerformed
    public void setDis() {
        disProduct();
        disAll();
        discountUnit();
    }

    private void discountUnit() {
        ArrayList<RecieptDetail> recieptDetails = reciept.getRecieptDetails();
        int unit = checkUnit();
        if (unit != -1) {
            discount = 0;
            //เรียงลำดับ recieptDetails ตามราคามากไปน้อย
            Collections.sort(recieptDetails, new Comparator<RecieptDetail>() {
                @Override
                public int compare(RecieptDetail r1, RecieptDetail r2) {
                    return Float.compare(r2.getProductPrice(), r1.getProductPrice());
                }
            });
            
            for (RecieptDetail r : recieptDetails) {
                if (unit == 0) {
                    break;
                }
                if (r.getQty() >= unit) {
                    // คำนวณลดราคาจากสินค้าที่มีราคามากที่สุด
                    discount += (float) (r.getProductPrice() * 0.1 * unit);
                    break;
                } else {
                    discount += (float) (r.getProductPrice() * 0.1 * r.getQty());
                    unit -= r.getQty();
                }
                
            }
            setTextDis();
            
        }
    }

    private int checkUnit() {
        ArrayList<RecieptDetail> recieptDetails = reciept.getRecieptDetails();
        Collections.sort(promotions, new Comparator<Promotion>() {
                    @Override
                    public int compare(Promotion r1, Promotion r2) {
                        return Integer.compare(r2.getUnit(), r1.getUnit());
                    }
                });
        for (Promotion p : promotions) {
            if (p.getUnit() != 0) {
                int count = 0;
                for (RecieptDetail r : recieptDetails) {
                    count += r.getQty();
                    System.out.println(r);
                }
                if (count >= p.getUnit()) {
                    ready = false;
                    setTextDisName(p);
                    return p.getUnit();
                }
            }

        }
        return -1;
    }

    private void disProduct() {
        ArrayList<RecieptDetail> recieptDetails = reciept.getRecieptDetails();
        for (Promotion p : promotions) {
            discount = 0;
            if(!p.getProduct().equals("")){
                String[] nameProduct = p.getProduct().split(",");           
                if(nameProduct.length>1){
                    checkDisSet(nameProduct, recieptDetails, p);
                }else{
                    for (RecieptDetail r : recieptDetails) {
                        if (r.getProductName().equals(p.getProduct())) {
                            discount += (float) (r.getProductPrice() * p.getDiscount() * r.getQty());
                        }
                    }
                } 
            }setTextDisName(p);
            setTextDis();
            
        }
    }
    private void  checkDisSet(String[] nameProduct, ArrayList<RecieptDetail> recieptDetails, Promotion p) { 
        int count = 0;
        int disproduct =0;
        for (String name : nameProduct) {
            for (RecieptDetail r : recieptDetails) {
                if (r.getProductName().equals(name)) {
                    disproduct += (float) (r.getProductPrice()*p.getDiscount());
                    count+=1;
                    break;
                }
            }
        }
        if(count ==nameProduct.length){
            discount = disproduct;
            setTextDisName(p);
        setTextDis();
        }
      
    }

    private void setTextDis() {
        if (discount > dismax) {
            dismax = discount;
            lblDiscountName.setText("Name : "+promotion.getName());
            lblDiscount.setText("Discount : " + dismax);
        }

    }

    private void setTextDisName(Promotion p) {
        promotion = p;
    }

    private void disAll() {
        for (Promotion p : promotions) {
            if (p.getUnit() == 0) {
                if (p.getProduct().equals("")) {
                    total = reciept.getTotal();
                    discount = (float) (Math.floor(total * p.getDiscount()));
                    setTextDisName(p);
                    setTextDis();
                    
                }
            }
        }

    }

    private void clearReciept() {
        reciept = new Reciept();
        reciept.setUser(UserService.getCurrent());
        refreshReciept();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(PosFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(PosFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(PosFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(PosFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                User i = new User();
                i = UserService.current;
                new PosFrame(i).setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCalculate;
    private javax.swing.JButton btnMainManu;
    private javax.swing.JButton btnTest;
    private javax.swing.JButton jButton1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel lblDiscount;
    private javax.swing.JLabel lblDiscountName;
    private javax.swing.JLabel lblHeader;
    private javax.swing.JLabel lblStatus;
    private javax.swing.JLabel lblTime;
    private javax.swing.JLabel lblTotal;
    private javax.swing.JLabel lblUserName;
    private javax.swing.JLabel pCoffee;
    private javax.swing.JLabel pUser;
    private javax.swing.JScrollPane scrProductList;
    private javax.swing.JTable tblReciepDetail;
    // End of variables declaration//GEN-END:variables
    public void buy(Product product, int qty) {
        reciept.addRecieptDetail(product, qty);
        refreshReciept();
    }

//    private void RecieptDialog() {
//
//        RecieptDialog recieptDialog = new RecieptDialog();
//        recieptDialog.setLocationRelativeTo(this);
//        recieptDialog.setVisible(true);
//        recieptDialog.addWindowListener(new WindowAdapter() {
//
//        });
//    }
    @Override
    public void buy(Product product, int qty, String type, String sweet) {
        reciept.addRecieptDetail(product, qty, type, sweet);
        refreshReciept();
    }

}
