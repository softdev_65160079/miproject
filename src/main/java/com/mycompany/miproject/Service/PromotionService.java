/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.miproject.Service;


import com.mycompany.miproject.Dao.PromotionDao;
import com.mycompany.miproject.Dao.PromotionDetailDao;

import com.mycompany.miproject.model.Promotion;
import com.mycompany.miproject.model.PromotionDetail;
import java.util.List;

/**
 *
 * @author werapan
 */
public class PromotionService {
    
    
    public Promotion getById(int id){
           PromotionDao PromotionDao = new PromotionDao();
            return PromotionDao.get(id);
       }
    public List<Promotion> getPromotions(){
        PromotionDao PromotionDao = new PromotionDao();
        return PromotionDao.getAll(" promotion_id asc");
    }

    
    public Promotion addNew(Promotion editedPromotion) {
        PromotionDao recieptDao = new PromotionDao();
        PromotionDetailDao recieptDetailDao = new PromotionDetailDao();
        Promotion reciept = recieptDao.save(editedPromotion);
        
        for(PromotionDetail rd:editedPromotion.getPromotionDetails()){
            rd.setPromotionId(reciept.getId());
            recieptDetailDao.save(rd);
        }
        return reciept;
    }

    public Promotion update(Promotion editedPromotion) {
        PromotionDao recieptDao = new PromotionDao();
        return recieptDao.update(editedPromotion);
    }

    public int delete(Promotion editedPromotion) {
        PromotionDao recieptDao = new PromotionDao();
        return recieptDao.delete(editedPromotion);
    }
}
