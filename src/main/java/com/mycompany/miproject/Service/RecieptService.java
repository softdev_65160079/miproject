/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.miproject.Service;

import com.mycompany.miproject.Dao.RecieptDao;
import com.mycompany.miproject.Dao.RecieptDetailDao;
import com.mycompany.miproject.model.Reciept;
import com.mycompany.miproject.model.RecieptDetail;
import java.util.List;

/**
 *
 * @author werapan
 */
public class RecieptService {

       public Reciept getById(int id){
           RecieptDao RecieptDao = new RecieptDao();
            return RecieptDao.get(id);
       }
    public List<Reciept> getReciepts(){
        RecieptDao RecieptDao = new RecieptDao();
        return RecieptDao.getAll(" reciept_id asc");
    }

    
    public Reciept addNew(Reciept editedReciept) {
        RecieptDao recieptDao = new RecieptDao();
        RecieptDetailDao recieptDetailDao = new RecieptDetailDao();
        Reciept reciept = recieptDao.save(editedReciept);
        
        for(RecieptDetail rd:editedReciept.getRecieptDetails()){
            rd.setRecieptId(reciept.getId());
            recieptDetailDao.save(rd);
        }
        return reciept;
    }

    public Reciept update(Reciept editedReciept) {
        RecieptDao recieptDao = new RecieptDao();
        return recieptDao.update(editedReciept);
    }

    public int delete(Reciept editedReciept) {
        RecieptDao recieptDao = new RecieptDao();
        return recieptDao.delete(editedReciept);
    }
}
