/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.miproject.Service;

import com.mycompany.miproject.Dao.ProductDao;
import com.mycompany.miproject.model.Product;
import java.util.ArrayList;

/**
 *
 * @author pattarapon
 */
public class ProductService {
    private final ProductDao productDao = new ProductDao();
    public ArrayList<Product> getProductsOrderByName(){
        return (ArrayList<Product>) productDao.getAll("product_name ASC");
    }
}
