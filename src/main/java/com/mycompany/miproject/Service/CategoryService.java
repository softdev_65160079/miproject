/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.miproject.Service;

import com.mycompany.miproject.Dao.CategoryDao;
import com.mycompany.miproject.model.Category;
import java.util.List;

/**
 *
 * @author pattarapon
 */
public class CategoryService {
    public Category getById(int id) {
        CategoryDao categoryDao = new CategoryDao();
        Category category = categoryDao.get(id);
        return category;
    }
    public List<Category> getCategorys(){
        CategoryDao CategoryDao = new CategoryDao();
        return CategoryDao.getAll(" category_id asc");
    }

    public Category addNew(Category editedCategory) {
        CategoryDao categoryDao = new CategoryDao();
        return categoryDao.save(editedCategory);
    }

    public Category update(Category editedCategory) {
        CategoryDao categoryDao = new CategoryDao();
        return categoryDao.update(editedCategory);
    }

    public int delete(Category editedCategory) {
        CategoryDao categoryDao = new CategoryDao();
        return categoryDao.delete(editedCategory);
    }
}