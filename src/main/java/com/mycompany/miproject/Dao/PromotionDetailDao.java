/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.miproject.Dao;

import com.mycompany.miproject.helper.DatabaseHelper;
import com.mycompany.miproject.model.PromotionDetail;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author werapan
 */
public class PromotionDetailDao implements Dao<PromotionDetail> {

    @Override
    public PromotionDetail get(int id) {
        PromotionDetail promotionDetail = null;
        String sql = "SELECT * FROM promotion_detail WHERE promotion_detail_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                promotionDetail = PromotionDetail.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return promotionDetail;
    }

    public List<PromotionDetail> getAll() {
        ArrayList<PromotionDetail> list = new ArrayList();
        String sql = "SELECT * FROM promotion_detail";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                PromotionDetail promotionDetail = PromotionDetail.fromRS(rs);
                list.add(promotionDetail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    @Override
    public List<PromotionDetail> getAll(String where, String order) {
        ArrayList<PromotionDetail> list = new ArrayList();
        String sql = "SELECT * FROM promotion_detail where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                PromotionDetail promotionDetail = PromotionDetail.fromRS(rs);
                list.add(promotionDetail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    

    public List<PromotionDetail> getAll(String order) {
        ArrayList<PromotionDetail> list = new ArrayList();
        String sql = "SELECT * FROM promotion_detail  ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                PromotionDetail promotionDetail = PromotionDetail.fromRS(rs);
                list.add(promotionDetail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public PromotionDetail save(PromotionDetail obj) {

        String sql = "INSERT INTO promotion_detail (promotion_id,product_id,promotion_discount)"
                + "VALUES(?,?,?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getPromotionId());
            stmt.setInt(2, obj.getProductId());
            stmt.setFloat(3, obj.getDiscount());
//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public PromotionDetail update(PromotionDetail obj) {
        String sql = "UPDATE promotion_detail"
                + " SET promotion_id=?,product_id= ? , promotion_discount= ? "
                + " WHERE promotion_detail_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
           stmt.setInt(1, obj.getPromotionId());
            stmt.setInt(2, obj.getProductId());
            stmt.setFloat(3, obj.getDiscount());
            stmt.setInt(4, obj.getId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }


    @Override
    public int delete(PromotionDetail obj) {
        String sql = "DELETE FROM promotion_detail WHERE promotion_detail_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;        
    }

}
